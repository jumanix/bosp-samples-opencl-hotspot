
ifdef CONFIG_SAMPLES_OPENCL_HOTSPOT

# Targets provided by this project
.PHONY: samples_opencl_hotspot clean_samples_opencl_hotspot

# Add this to the "samples_opencl" target
samples_opencl: samples_opencl_hotspot
clean_samples_opencl: clean_samples_opencl_hotspot

MODULE_SAMPLES_OPENCL_HOTSPOT=samples/opencl/hotspot

samples_opencl_hotspot:
	@echo
	@echo "==== Building HotSpot ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_SAMPLES_OPENCL_HOTSPOT)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_SAMPLES_OPENCL_HOTSPOT)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_SAMPLES_OPENCL_HOTSPOT)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_SAMPLES_OPENCL_HOTSPOT)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_samples_opencl_hotspot:
	@echo
	@echo "==== Clean-up HotSpot Application ===="
	@rm -rf $(MODULE_SAMPLES_OPENCL_HOTSPOT)/build
	@echo

run-ocl-hotspot: samples_opencl_hotspot
	@echo === Running HotSpot OCL ===
	$(BOSP_SYSROOT)/usr/bin/hotspot 512 2 2000 \
	$(BOSP_SYSROOT)/usr/share/bbque/hotspot/data/hotspot/temp_512 \
	$(BOSP_SYSROOT)/usr/share/bbque/hotspot/data/hotspot/power_512 output.out

else # CONFIG_SAMPLES_OPENCL_HOTSPOT

samples_opencl_hotspot:
	$(warning HotSpot disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_SAMPLES_OPENCL_HOTSPOT


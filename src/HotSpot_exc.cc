/**
 *       @file  HotSpot_exc.cc
 *      @brief  The HotSpot BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Name Surname (nickname), your@email.com
 *
 *     Company  Your Company
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */


#include "HotSpot_exc.h"
#include "hotspot.h"

#include <cstdio>
#include <iostream>
#include <bbque/utils/utility.h>


/**********************************************
 *            Utility methods                 *
 **********************************************/

void writeoutput(float *vect, int grid_rows, int grid_cols, char *file)
{
	int i,j, index=0;
	FILE *fp;
	char str[STR_SIZE];

	if( (fp = fopen(file, "w" )) == 0 ) {
		std::cout << "The file was not opened\n";
		return;
	}


	for (i=0; i < grid_rows; i++)
		for (j=0; j < grid_cols; j++) {
			sprintf(str, "%d\t%g\n", index, vect[i*grid_cols+j]);
			fputs(str,fp);
			index++;
		}
	fclose(fp);
}


void readinput(float *vect, int grid_rows, int grid_cols, char *file)
{
  	int i,j;
	FILE *fp;
	char str[STR_SIZE];
	float val;

	if( (fp  = fopen(file, "r" )) ==0 )
		std::cout << "The file was not opened";

	for (i=0; i <= grid_rows-1; i++)
		for (j=0; j <= grid_cols-1; j++)
		{
			if (fgets(str, STR_SIZE, fp) == NULL)
				std::cout << "Error reading file\n";
			if (feof(fp))
				std::cout << "not enough lines in file\n";
			//if ((sscanf(str, "%d%f", &index, &val) != 2) || (index != ((i-1)*(grid_cols-2)+j-1)))
			if ((sscanf(str, "%f", &val) != 1))
				std::cout <<"invalid file format\n";
			vect[i*grid_cols+j] = val;
		}
	fclose(fp);
}


/**********************************************
 *           Hotspot class                    *
 **********************************************/


HotSpot::HotSpot(std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib,
		int _grid_rows,
		int _grid_cols,
		int _total_iterations,
		int _pyramid_height,
		char * _tfile,
		char * _pfile,
		char * _ofile,
		int awm) :
	grid_rows(_grid_rows),
	grid_cols(_grid_cols),
	total_iterations(_total_iterations),
	pyramid_height(_pyramid_height),
	tfile(_tfile),
	pfile(_pfile),
	ofile(_ofile),
	awm_id(awm),
	BbqueEXC(name, recipe, rtlib, RTLIB_LANG_OPENCL) {

	logger->Warn("New HotSpot::HotSpot()");

	// NOTE: since RTLib 1.1 the derived class construct should be used
	// mainly to specify instantiation parameters. All resource
	// acquisition, especially threads creation, should be palced into the
	// new onSetup() method.

	logger->Info("EXC Unique IDentifier (UID): %u", GetUid());
	size = grid_rows * grid_cols;
}

RTLIB_ExitCode_t HotSpot::onSetup() {
	// This is just an empty method in the current implementation of this
	// testing application. However, this is intended to collect all the
	// application specific initialization code, especially the code which
	// acquire system resources (e.g. thread creation)
	logger->Warn("HotSpot::onSetup()");

	// --------------- pyramid parameters ---------------
	borderCols    = (pyramid_height)*EXPAND_RATE/2;
	borderRows    = (pyramid_height)*EXPAND_RATE/2;
	smallBlockCol = BLOCK_SIZE-(pyramid_height)*EXPAND_RATE;
	smallBlockRow = BLOCK_SIZE-(pyramid_height)*EXPAND_RATE;
	blockCols     = grid_cols/smallBlockCol+((grid_cols%smallBlockCol==0)?0:1);
	blockRows     = grid_rows/smallBlockRow+((grid_rows%smallBlockRow==0)?0:1);

	FilesavingTemp  = (float *) malloc(size*sizeof(float));
	FilesavingPower = (float *) malloc(size*sizeof(float));
	// MatrixOut = (float *) calloc (size, sizeof(float));

	if( !FilesavingPower || !FilesavingTemp) // || !MatrixOut)
		logger->Error("unable to allocate memory");

	// Read input data from disk
	readinput(FilesavingTemp,  grid_rows, grid_cols, tfile);
	readinput(FilesavingPower, grid_rows, grid_cols, pfile);

	// Force AWM (profiling case)
	if (awm_id >= 0) {
		RTLIB_Constraint_t constr_set = {
			awm_id,
			CONSTRAINT_ADD,
			UPPER_BOUND
		};
		SetConstraints(&constr_set, 1);
		logger->Notice("Forcing Application Working Mode [%2d]", awm_id);
	}
	return RTLIB_OK;
}

RTLIB_ExitCode_t HotSpot::onConfigure(int8_t awm_id) {

	/*** OpenCL device ***/
	cl_int error;
	cl_uint num_platforms;

	logger->Warn("HotSpot::onConfigure(): EXC [%s] => AWM [%02d]",
		exc_name.c_str(), awm_id);
	if (Cycles() > 0) {
		writeoutput(MatrixOut, grid_rows, grid_cols, ofile);
		clCleanup();
	}

	// Get the number of platforms
	error = clGetPlatformIDs(0, NULL, &num_platforms);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);

	// Get the list of platforms
	cl_platform_id* platforms = (cl_platform_id *) malloc(sizeof(cl_platform_id) * num_platforms);
	error = clGetPlatformIDs(num_platforms, platforms, NULL);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);

	// Print the chosen platform (if there are multiple platforms, choose the first one)
	cl_platform_id platform = platforms[0];
	char pbuf[100];
	error = clGetPlatformInfo(platform, CL_PLATFORM_VENDOR, sizeof(pbuf), pbuf, NULL);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);
	printf("Platform: %s\n", pbuf);

	// Create a GPU context
	cl_context_properties context_properties[3] = { CL_CONTEXT_PLATFORM, (cl_context_properties) platform, 0};
	context = clCreateContextFromType(context_properties, CL_DEVICE_TYPE_ALL, NULL, NULL, &error);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);

	// Get and print the chosen device (if there are multiple devices, choose the first one)
	size_t devices_size;
	error = clGetContextInfo(context, CL_CONTEXT_DEVICES, 0, NULL, &devices_size);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	cl_device_id *devices = (cl_device_id *) malloc(devices_size);
	error = clGetContextInfo(context, CL_CONTEXT_DEVICES, devices_size, devices, NULL);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	device = devices[0];

	// Create a command queue
	command_queue = clCreateCommandQueue(context, device, 0, &error);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);

	/***  Parameters ***/

	grid_height = chip_height / grid_cols;
	grid_width  = chip_width  / grid_rows;
	Cap = FACTOR_CHIP * SPEC_HEAT_SI * t_chip * grid_width * grid_height;
	Rx  = grid_width  / (2.0 * K_SI * t_chip      * grid_height);
	Ry  = grid_height / (2.0 * K_SI * t_chip      * grid_width);
	Rz  = t_chip      / (      K_SI * grid_height * grid_width);
	max_slope = MAX_PD    / (FACTOR_CHIP * t_chip * SPEC_HEAT_SI);
	step      = PRECISION / max_slope;

	// Determine GPU work group grid
	global_work_size[0] = BLOCK_SIZE * blockCols;
	global_work_size[1] = BLOCK_SIZE * blockRows;
	local_work_size[0]  = BLOCK_SIZE;
	local_work_size[1]  = BLOCK_SIZE;
	logger->Info("[hotspot] global work: %d, local work: %d, # workgroups: %d\n",
		global_work_size[0] * global_work_size[1],
		local_work_size[0]  * local_work_size[1],
		(global_work_size[0]*global_work_size[1])/(local_work_size[0]*local_work_size[1]));

	/*** Kernel building */

	// Load kernel source from file
	char kernel_path[] = KERNEL_PATH"/hotspot_kernel.cl";
	logger->Warn("[hotspot] Loading kernel from %s", kernel_path); 
	const char *source = load_kernel_source(kernel_path);
	size_t sourceSize  = strlen(source);
	cl_program program = clCreateProgramWithSource(context, 1, &source, &sourceSize, &error);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);

	char clOptions[110];
	//  sprintf(clOptions,"-I../../src");
	sprintf(clOptions," ");
#ifdef BLOCK_SIZE
	sprintf(clOptions + strlen(clOptions), " -DBLOCK_SIZE=%d", BLOCK_SIZE);
#endif
	// Create an executable from the kernel
	error = clBuildProgram(program, 1, &device, clOptions, NULL, NULL);
	// Show compiler warnings/errors
	static char log[65536]; memset(log, 0, sizeof(log));
	clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, sizeof(log)-1, log, NULL);
	if (strstr(log,"warning:") || strstr(log, "error:")) printf("<<<<\n%s\n>>>>\n", log);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);
	kernel = clCreateKernel(program, "hotspot", &error);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);

//	long long start_time = get_time();

	// Create two temperature matrices and copy the temperature input data
	// Create input memory buffers on device
	MatrixTemp[0] = clCreateBuffer(
		context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
		sizeof(float) * size, FilesavingTemp, &error);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);
	logger->Info("Buffer created: %d KB\n", sizeof(float) * size / 1024);

	MatrixTemp[1] = clCreateBuffer(
		context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR,
		sizeof(float) * size, NULL, &error);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);
	logger->Info("Buffer created: %d KB\n", sizeof(float) * size / 1024);

	// Copy the power input data
	MatrixPower = clCreateBuffer(
		context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
		sizeof(float) * size, FilesavingPower, &error);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);
	logger->Info("Buffer created: %d KB\n", sizeof(float) * size / 1024);

	return RTLIB_OK;
}

RTLIB_ExitCode_t HotSpot::onRun() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	// Return after 5 cycles
	if (Cycles() >= total_iterations)
		return RTLIB_EXC_WORKLOAD_NONE;

	// Do one more cycle
	logger->Warn("HotSpot::onRun()      : EXC [%s]  @ AWM [%02d]",
		exc_name.c_str(), wmp.awm_id);

	// Perform the computation
	src_mat = compute_tran_temp();

	// Copy final temperature data back
	cl_int error;
	MatrixOut = (cl_float *) clEnqueueMapBuffer(
			command_queue,
			MatrixTemp[src_mat],
			CL_TRUE, CL_MAP_READ,
			0, sizeof(float) * size, 0, NULL, NULL, &error);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);
	printf("Buffer created: %d KB\n", sizeof(float) * size / 1024);

	return RTLIB_OK;
}

RTLIB_ExitCode_t HotSpot::onMonitor() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	logger->Warn("HotSpot::onMonitor()  : EXC [%s]  @ AWM [%02d], Cycle [%4d]",
		exc_name.c_str(), wmp.awm_id, Cycles());

	return RTLIB_OK;
}

RTLIB_ExitCode_t HotSpot::onRelease() {
	logger->Warn("HotSpot::onRelease()  : exit");

	// Write final output to output file
	writeoutput(MatrixOut, grid_rows, grid_cols, ofile);
	clCleanup();

	free(FilesavingTemp);
	free(FilesavingPower);

	return RTLIB_OK;
}

void HotSpot::clCleanup() {
	cl_int error = clEnqueueUnmapMemObject(
		command_queue, MatrixTemp[src_mat], (void *) MatrixOut, 0, NULL, NULL);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);

	clReleaseMemObject(MatrixTemp[0]);
	clReleaseMemObject(MatrixTemp[1]);
	clReleaseMemObject(MatrixPower);
	clReleaseContext(context);
}

int HotSpot::compute_tran_temp(){
	int src = 0, dst = 1;
	cl_int error;

	long long start_time = get_time();

	// Specify kernel arguments
	int iter = MIN(pyramid_height, total_iterations - Cycles());
	clSetKernelArg(kernel, 0,  sizeof(int), (void *) &iter);
	clSetKernelArg(kernel, 1,  sizeof(cl_mem), (void *) &MatrixPower);
	clSetKernelArg(kernel, 2,  sizeof(cl_mem), (void *) &MatrixTemp[src]);
	clSetKernelArg(kernel, 3,  sizeof(cl_mem), (void *) &MatrixTemp[dst]);
	clSetKernelArg(kernel, 4,  sizeof(int),   (void *) &grid_rows);
	clSetKernelArg(kernel, 5,  sizeof(int),   (void *) &grid_cols);
	clSetKernelArg(kernel, 6,  sizeof(int),   (void *) &borderCols);
	clSetKernelArg(kernel, 7,  sizeof(int),   (void *) &borderRows);
	clSetKernelArg(kernel, 8,  sizeof(float), (void *) &Cap);
	clSetKernelArg(kernel, 9,  sizeof(float), (void *) &Rx);
	clSetKernelArg(kernel, 10, sizeof(float), (void *) &Ry);
	clSetKernelArg(kernel, 11, sizeof(float), (void *) &Rz);
	clSetKernelArg(kernel, 12, sizeof(float), (void *) &step);

	// Launch kernel
	error = clEnqueueNDRangeKernel(
		command_queue, kernel, 2, NULL,
		global_work_size, local_work_size, 0, NULL, NULL);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);

	// Flush the queue
	error = clFlush(command_queue);
	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);

	// Swap input and output GPU matrices
	src = 1 - src;
	dst = 1 - dst;

	// Wait for all operations to finish
//	error = clFinish(command_queue);
//	if (error != CL_SUCCESS) fatal_CL(error, __LINE__);

	long long end_time = get_time();
	long long total_time = (end_time - start_time);
	logger->Info("\nKernel time: %.3f seconds\n", ((float) total_time) / (1000*1000));

	return src;
}



cmake_minimum_required(VERSION 3.0)

set (BOSP_SYSROOT ${CMAKE_INSTALL_PREFIX})
find_package(BbqRTLib REQUIRED)

#----- Add compilation dependencies
include_directories(${BBQUE_RTLIB_INCLUDE_DIR})

#----- Add "hotspot" target application
set(HOTSPOT_SRC version HotSpot_exc HotSpot_main OpenCL_helper_library)
add_executable(hotspot ${HOTSPOT_SRC})

#----- Linking dependencies
target_link_libraries(
	hotspot
	${Boost_LIBRARIES}
	${BBQUE_RTLIB_LIBRARY}
)

# Link against the vendor OpenCL library
if (NOT CONFIG_BBQUE_OPENCL)
  find_package (OpenCL REQUIRED)
  target_link_libraries(
	hotspot
	${OPENCL_LIBRARIES}
  )
endif (NOT CONFIG_BBQUE_OPENCL)

include_directories (${OPENCL_INCLUDE_DIR})

# Use link path ad RPATH
set_property(TARGET hotspot PROPERTY
	INSTALL_RPATH_USE_LINK_PATH TRUE)

#----- Install the HotSpot files
install (TARGETS hotspot RUNTIME
	DESTINATION ${HOTSPOT_PATH_BINS})

#----- Generate and Install HotSpot configuration file
configure_file (
	"${PROJECT_SOURCE_DIR}/HotSpot.conf.in"
	"${PROJECT_BINARY_DIR}/HotSpot.conf"
)
install (FILES "${PROJECT_BINARY_DIR}/HotSpot.conf"
	DESTINATION ${HOTSPOT_PATH_CONFIG})

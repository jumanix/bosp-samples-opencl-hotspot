/**
 *       @file  HotSpot_exc.h
 *      @brief  The HotSpot BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Name Surname (nickname), your@email.com
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef HOTSPOT_EXC_H_
#define HOTSPOT_EXC_H_

#include <bbque/bbque_exc.h>

#ifndef CONFIG_BBQUE_OPENCL
#include <CL/cl.h>
#endif

using bbque::rtlib::BbqueEXC;

class HotSpot : public BbqueEXC {

public:

	HotSpot(std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib,
		int _grid_rows,
		int _grid_cols,
		int _total_iterations,
		int _pyramid_height,
		char * _tfile,
		char * _pfile,
		char * _ofile,
		int awm = -1
	);

private:

	int size;
	int grid_rows = 0;
	int grid_cols = 0;
	int total_iterations = 60;
	int pyramid_height   = 1;
	char *tfile;
	char *pfile;
	char *ofile;

	int awm_id;

	// --------------- pyramid parameters ---------------
	int borderCols;
	int borderRows;
	int smallBlockCol;
	int smallBlockRow;
	int blockCols;
	int blockRows;
	float *FilesavingTemp;
	float *FilesavingPower;
	cl_float * MatrixOut;
	cl_mem MatrixTemp[2];
	cl_mem MatrixPower;

	float grid_height;
	float grid_width;
	float Cap;
	float Rx;
	float Ry;
	float Rz;
	float max_slope;
	float step;

	size_t global_work_size[2];
	size_t local_work_size[2];

	int src_mat;

	// OpenCL globals
	cl_context context;
	cl_command_queue command_queue;
	cl_device_id device;
	cl_kernel kernel;

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(int8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onRelease();

	int compute_tran_temp();

	void clCleanup();
};

#endif // HOTSPOT_EXC_H_
